FROM bellsoft/liberica-openjdk-debian:11
RUN apt update
RUN apt install -y git
WORKDIR /data
COPY build.sh /data/build.sh
COPY entrypoint.sh /data/entrypoint.sh
RUN ["bash", "/data/build.sh"]
ENTRYPOINT ["bash", "/data/entrypoint.sh"]